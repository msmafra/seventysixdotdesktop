#!/usr/bin/env bash
#################################################################
#                                                               #
#            Seventy Six dot Desktop Installer                  #
#                      Version: 1.0.0                           #
#                    Copyright (C) 2021                         #
#           Marcelo dos Santos Mafra <msmafra@gmail.com>        #
#       Licensed under the GNU General Public License v3.0      #
#                                                               #
#        https://gitlab.com/msmafra/seventysixdotdesktop        #
#                                                               #
#################################################################
#{{{ Bash settings
set -o errexit   # abort on nonzero exit status
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes
set -o errtrace  # ERR trap is inherited by shell functions
#set -o xtrace    # show commands as they are executed
#}}}

main() {
    coprRepo
    installScript
    postInstall
}

postInstall() {
    printf "\n%s\n" "-- Make sure everything is saved --"
    read -p "Gnome can take a while to update the dock. Do you want this script to restart(refresh) without loging off (Xorg only)? " -n 1 -r
    if [[ "${REPLY}" =~ ^[Yy]$ ]]
    then
        printf "\n%s\n" "-- Refreshing your current Gnome session --"
        killall --verbose -SIGQUIT gnome-shell
        printf "\n%s\n" "Session refreshed. Bye!"
    else
        printf "%s\n:%s\n"  "To restart of Gnome's session, Xorg only, without log off" "ALT + F2, type 'r' and press Enter/Return"
    fi
}

coprRepo() {
    local checkInstall
    checkInstall="$(command -pv system76-power)"

    if [[ ! "${checkInstall}" ]];then
        printf "system76-power is not installed. Check the COPR repo in:\n%s" "https://copr.fedorainfracloud.org/coprs/szydell/system76/"
    fi
}

installScript() {
    local -i userId
    local userName
    local -i scriptMode
    local -i desktopMode
    local tmpFolder
    local desktopFile
    userId="$(id --user)"
    userName="$(id --user --name)"
    scriptMode="740"
    desktopMode="644"
    tmpFolder="$(mktemp --suffix=SEVENTYSIX --directory)"
    desktopFile="seventysix.desktop"
    scriptFolder="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"

    if [[ ${userId} -ne 0 ]];then # No super user

        printf "%s\n" "Copying files to ${tmpFolder}..."
        cp --verbose "${scriptFolder}"/setup.sh "${scriptFolder}"/seventysix "${scriptFolder}"/"${desktopFile}" "${tmpFolder}"

        printf "%s\n" "Changing to ${tmpFolder}..."
        cd "${tmpFolder}"

        printf "%s\n" "Configuring ${desktopFile}..."
        sed -i "s#Exec=~/.local/bin/seventysix.sh#Exec=$HOME/.local/bin/seventysix#" "${desktopFile}"

        printf "%s\n" "Installing files..."
        install --verbose -D --mode="${scriptMode}" --owner="${userName}" --group="${userName}" "${tmpFolder}"/seventysix --target-directory="${HOME}"/.local/bin
        install --verbose -D --mode="${desktopMode}" --owner="${userName}" --group="${userName}" "${tmpFolder}"/"${desktopFile}" --target-directory="${HOME}"/.local/share/applications/

        printf "\n%s\n" "Adding seventysix.desktop to Gnome's dock Favorites"
        printf "%s\n" "Your current favorites"
        gsettings get org.gnome.shell favorite-apps
        printf "%s\n" "----------------------"
        if  gsettings get org.gnome.shell favorite-apps 2>&1 | grep --quiet "${desktopFile}";then
            printf "%s\n" "${desktopFile} already in favorites"
        else
            gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), '${desktopFile}']"
            printf "New favorites with %s\n" "${desktopFile}"
            gsettings get org.gnome.shell favorite-apps
        fi
    else
        exit 1
    fi
}

main "${@}"
