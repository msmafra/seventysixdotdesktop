# Seventy Six dot Desktop
It is just a lazy jenky script to use https://copr.fedorainfracloud.org/coprs/szydell/system76/ on Fedora without going to the command line while in Gnome Shell.

![Seventy Six Desktop right click](./seventysixdesktop.png?raw=true "Seventy Six Dekstop Right Click")

## Install form a terminal
```
./setup.sh
```

## What?
It will put the _seventysix_ bash script inside your $HOME/.local/bin and add a seventysix.desktop to your /.local/share/applications and also add it to your dock favorites.

## Using
The seventysix.desktop gives access to some of _system75-power_ sub commands via context meu (right clicking).

### Available sub commands

- profile
- graphics integrated
- graphics compute
- graphics nvidia
- graphics hybrid
- reboot
